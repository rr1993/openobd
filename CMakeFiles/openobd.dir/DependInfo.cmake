# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/robert/Downloads/openobd-0.5.0/src/dlgOptions.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/dlgOptions.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/elm327.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/elm327.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/gui.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/gui.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/logPanel.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/logPanel.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/main.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/main.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/milPanel.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/milPanel.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/obdFrame.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/obdFrame.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/obdbase.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/obdbase.cpp.o"
  "/home/robert/Downloads/openobd-0.5.0/src/pidPanel.cpp" "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/openobd.dir/src/pidPanel.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "WXUSINGDLL"
  "_FILE_OFFSET_BITS=64"
  "__WXGTK__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/lib/wx/include/gtk2-unicode-3.0"
  "/usr/include/wx-3.0"
  "include"
  "src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/robert/Downloads/openobd-0.5.0/CMakeFiles/CTB.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
