# This file will be configured to contain variables for CPack. These variables
# should be set in the CMake list file of the project before CPack module is
# included. The list of available CPACK_xxx variables and their associated
# documentation may be obtained using
#  cpack --help-variable-list
#
# Some variables are common to all generators (e.g. CPACK_PACKAGE_NAME)
# and some are specific to a generator
# (e.g. CPACK_NSIS_EXTRA_INSTALL_COMMANDS). The generator specific variables
# usually begin with CPACK_<GENNAME>_xxxx.


SET(CPACK_BINARY_7Z "")
SET(CPACK_BINARY_BUNDLE "")
SET(CPACK_BINARY_CYGWIN "")
SET(CPACK_BINARY_DEB "")
SET(CPACK_BINARY_DRAGNDROP "")
SET(CPACK_BINARY_IFW "")
SET(CPACK_BINARY_NSIS "")
SET(CPACK_BINARY_OSXX11 "")
SET(CPACK_BINARY_PACKAGEMAKER "")
SET(CPACK_BINARY_PRODUCTBUILD "")
SET(CPACK_BINARY_RPM "")
SET(CPACK_BINARY_STGZ "")
SET(CPACK_BINARY_TBZ2 "")
SET(CPACK_BINARY_TGZ "")
SET(CPACK_BINARY_TXZ "")
SET(CPACK_BINARY_TZ "")
SET(CPACK_BINARY_WIX "")
SET(CPACK_BINARY_ZIP "")
SET(CPACK_BUILD_SOURCE_DIRS "/home/robert/Downloads/openobd-0.5.0;/home/robert/Downloads/openobd-0.5.0")
SET(CPACK_CMAKE_GENERATOR "Unix Makefiles")
SET(CPACK_COMPONENT_UNSPECIFIED_HIDDEN "TRUE")
SET(CPACK_COMPONENT_UNSPECIFIED_REQUIRED "TRUE")
SET(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "libwxgtk2.8-0 (>= 2.8.7.1), libsqlite3-0 (>= 3.6)")
SET(CPACK_DEBIAN_PACKAGE_SECTION "Miscellaneous")
SET(CPACK_DEBIAN_PACKAGE_VERSION "0.5.0")
SET(CPACK_GENERATOR "DEB;RPM")
SET(CPACK_INSTALL_CMAKE_PROJECTS "/home/robert/Downloads/openobd-0.5.0;openobd;ALL;/")
SET(CPACK_INSTALL_PREFIX "/usr/local")
SET(CPACK_MODULE_PATH "/home/robert/Downloads/openobd-0.5.0")
SET(CPACK_NSIS_DISPLAY_NAME "openobd 0.5.0")
SET(CPACK_NSIS_INSTALLER_ICON_CODE "")
SET(CPACK_NSIS_INSTALLER_MUI_ICON_CODE "")
SET(CPACK_NSIS_INSTALL_ROOT "$PROGRAMFILES")
SET(CPACK_NSIS_PACKAGE_NAME "openobd 0.5.0")
SET(CPACK_OUTPUT_CONFIG_FILE "/home/robert/Downloads/openobd-0.5.0/CPackConfig.cmake")
SET(CPACK_PACKAGE_CONTACT "Simon Booth <simesb@users.sourceforge.net>")
SET(CPACK_PACKAGE_DEFAULT_LOCATION "/")
SET(CPACK_PACKAGE_DESCRIPTION "OBD-II communications")
SET(CPACK_PACKAGE_DESCRIPTION_FILE "/usr/share/cmake-3.8/Templates/CPack.GenericDescription.txt")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "openOBD: OBD-II interface control")
SET(CPACK_PACKAGE_FILE_NAME "openobd-0.5.0")
SET(CPACK_PACKAGE_INSTALL_DIRECTORY "openobd 0.5.0")
SET(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "openobd 0.5.0")
SET(CPACK_PACKAGE_NAME "openobd")
SET(CPACK_PACKAGE_RELOCATABLE "true")
SET(CPACK_PACKAGE_VENDOR "Simon Booth")
SET(CPACK_PACKAGE_VERSION "0.5.0")
SET(CPACK_PACKAGE_VERSION_MAJOR "0")
SET(CPACK_PACKAGE_VERSION_MINOR "5")
SET(CPACK_PACKAGE_VERSION_PATCH "0")
SET(CPACK_RESOURCE_FILE_LICENSE "/home/robert/Downloads/openobd-0.5.0/data/installlicense.txt")
SET(CPACK_RESOURCE_FILE_README "/usr/share/cmake-3.8/Templates/CPack.GenericDescription.txt")
SET(CPACK_RESOURCE_FILE_WELCOME "/usr/share/cmake-3.8/Templates/CPack.GenericWelcome.txt")
SET(CPACK_RPM_PACKAGE_ARCHITECTURE "i386")
SET(CPACK_RPM_PACKAGE_REQUIRES "wxGTK gettext sqlite3")
SET(CPACK_RPM_PACKAGE_VERSION "0.5.0")
SET(CPACK_SET_DESTDIR "ON")
SET(CPACK_SOURCE_7Z "")
SET(CPACK_SOURCE_CYGWIN "")
SET(CPACK_SOURCE_GENERATOR "ZIP;TGZ")
SET(CPACK_SOURCE_IGNORE_FILES "/.git;/build;/po/openobd.pot;/Output;/sqlite;/docs")
SET(CPACK_SOURCE_OUTPUT_CONFIG_FILE "/home/robert/Downloads/openobd-0.5.0/CPackSourceConfig.cmake")
SET(CPACK_SOURCE_PACKAGE_FILE_NAME "openobd-0.5.0")
SET(CPACK_SOURCE_RPM "")
SET(CPACK_SOURCE_TBZ2 "")
SET(CPACK_SOURCE_TGZ "")
SET(CPACK_SOURCE_TXZ "")
SET(CPACK_SOURCE_TZ "")
SET(CPACK_SOURCE_ZIP "")
SET(CPACK_STRIP_FILES "bin/openobd")
SET(CPACK_SYSTEM_NAME "Linux")
SET(CPACK_TOPLEVEL_TAG "Linux")
SET(CPACK_WIX_SIZEOF_VOID_P "8")

if(NOT CPACK_PROPERTIES_FILE)
  set(CPACK_PROPERTIES_FILE "/home/robert/Downloads/openobd-0.5.0/CPackProperties.cmake")
endif()

if(EXISTS ${CPACK_PROPERTIES_FILE})
  include(${CPACK_PROPERTIES_FILE})
endif()
